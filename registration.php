<?php
/**
 * @category  Beside
 * @package   Beside_Contact
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Beside_Contact', __DIR__);
