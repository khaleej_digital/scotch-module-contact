<?php
/**
 * @category  Beside
 * @package   Beside_Contact
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Contact\Block;

/**
 * Class ContactForm
 * @package Beside\Contact\Block
 */
class ContactForm extends \Magento\Contact\Block\ContactForm
{
    const XML_PATH_REASONS = 'beside_contact/general/reason';
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Framework\Serialize\SerializerInterface
     */
    private $serializer;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Serialize\SerializerInterface $serializer,
        array $data = []
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->serializer = $serializer;

        return parent::__construct($context,$data);
    }

    /**
     * @param string $template
     * @return ContactForm
     */
    public function setTemplate($template)
    {
        $template = 'Beside_Contact::form.phtml';

        return parent::setTemplate($template);
    }

    /**
     * @return array
     */
    public function getReasons()
    {
        $reasons[] = [
                'value' => '--Select Reason--',
                'label' => '--Select Reason--',
            ];
        $reasonSerialized = $this->scopeConfig->getValue(self::XML_PATH_REASONS);

        if(!$reasonSerialized){
            return $reasons;
        }

        foreach ($this->serializer->unserialize($reasonSerialized) as $reason) {
            $reasons[] = [
                'label' => $reason['reason'],
                'value' => $reason['reason']
            ];
        }

        return $reasons;
    }
}
